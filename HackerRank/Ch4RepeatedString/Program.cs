﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4RepeatedString
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine(repeatedString("aba", 10));
            Console.WriteLine(repeatedString("a", 10000));
            Console.ReadKey();
        }



        static long repeatedString(string s, long n)
        {

            //abaabababababababababababab
            //add until length> n
            //multiply occurence of a 
            //solve case when n*s.len mod n != 0
            if (s.Length > n)
                return CountAppereance(s.Substring(0, (int)n), 'a');

            long ret = CountAppereance(s, 'a') * (n / s.Length);

            if (n % s.Length > 0)
                ret += CountAppereance(s.Substring(0, (int)(n % s.Length)), 'a');

            return ret;
        }

        private static int CountAppereance(string s, char a)
        {
            int ret = 0;
            foreach (var c in s)
            {
                if (c == a)
                    ret++;
            }

            return ret;
        }
    }
}
