﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch3JumpingOnClouds
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(jumpingOnClouds(new int[] { 0, 0, 1, 0, 0, 1, 0 }));
            Console.WriteLine(jumpingOnClouds(new int[] { 0, 1, 0, 0, 0, 1, 0 }));
            Console.ReadKey();
        }

        static int jumpingOnClouds(int[] c)
        {
            int jump=0;
            int index=0;

            while (index + 1 < c.Length)
            {
                if (index + 2 < c.Length)
                {
                    if (c[index + 2] != 1)
                        index += 2;
                    else
                        index++;
                    jump++;
                }
                else if (index + 1 < c.Length)
                {
                    if (c[index + 1] != 1)
                        jump++;
                    index++;
                }
            }
            return jump;
        }
    }
}
