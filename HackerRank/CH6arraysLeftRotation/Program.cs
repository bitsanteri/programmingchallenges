﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH6arraysLeftRotation
{
    class Program
    {
        static void Main(string[] args)
        {
            var res = rotLeft(new int[5] { 1, 2, 3, 4, 5 }, 61);
            Console.WriteLine(string.Join(",", res.Select(p => p.ToString()).ToArray()));
            Console.ReadKey();
        }


        static int[] rotLeft(int[] a, int d)
        {
            var shift = d < a.Length ? d : d % a.Length;

            if (shift == 0)
                return a;

            int[] tempArr = new int[a.Length];

            for (int j = 0; j < a.Length; j++)
            {
                tempArr[j] = a[(j + shift) % a.Length];
            }

            //int[] tempArr = new int[a.Length];

            //for (int i = 0; i < d; i++)
            //{
            //    tempArr[tempArr.Length - 1] = a[0];

            //    for (int j = 0; j < a.Length-1; j++)
            //    {
            //        tempArr[j] = a[j + 1];
            //    }

            //    a = (int[])tempArr.Clone();
            //}


            return tempArr;
        }
    }
}
