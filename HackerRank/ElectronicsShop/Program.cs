﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicsShop
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(getMoneySpent(new int[] {3,1}, new int[] {5,2,8}, 10));
            Console.WriteLine(getMoneySpent(new int[] { 4 }, new int[] { 5 }, 5));
            Console.ReadKey();
        }

        static int getMoneySpent(int[] keyboards, int[] drives, int b)
        {
            int ret = -1;
            foreach (var k in keyboards)
            {
                foreach (var d in drives)
                {
                    var price = k + d;
                    if (price <= b && price > ret)
                    {
                        ret = price;
                    }
                }
            }


            return ret;
        }
    }
}
