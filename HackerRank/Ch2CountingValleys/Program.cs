﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch2CountingValleys
{
    class Program
    {
        static void Main(string[] args)
        {

            //TextWriter textWriter = new StreamWriter(@System.Environment.GetEnvironmentVariable("OUTPUT_PATH"), true);

            int n = 8;/*Convert.ToInt32(Console.ReadLine());*/

            string s = "UDDDUDUU";/*Console.ReadLine();*/
            //s = "DDUUDDUDUUUD";

            int result = countingValleys(n, s);

            Console.WriteLine("Valleys: " + result);
            Console.ReadKey();
            //textWriter.WriteLine(result);

            //textWriter.Flush();
            //textWriter.Close();

        }


        static int countingValleys(int n, string s)
        {
            int level=0;
            int valleyCnt = 0;
            bool valley = false;

            foreach (char c in s)
            {
                if (c == 'U')
                    level++;
                if (c == 'D')
                    level--;

                if (level < 0 && !valley)
                {
                    valleyCnt++;
                    valley = true;
                }
                if (level >= 0 && valley)
                {
                    valley = false;
                }
            }
            

            return valleyCnt;
        }
    }
}
