﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ch5_2dArray
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] matrix = new int[6][];
            //matrix[0] = new int[6] { 1, 1, 1, 0, 0, 0 };
            //matrix[1] = new int[6] { 0, 1, 0, 0, 0, 0 };
            //matrix[2] = new int[6] { 1, 1, 1, 0, 0, 0 };
            //matrix[3] = new int[6] { 0, 0, 2, 4, 4, 0 };
            //matrix[4] = new int[6] { 0, 0, 0, 2, 0, 0 };
            //matrix[5] = new int[6] { 0, 0, 1, 2, 4, 0 };

            matrix[0] = new int[6] { -1, -1, 0, -9, -2, -2 };
            matrix[1] = new int[6] { -2, -1, -6, -8, -2, -5 };
            matrix[2] = new int[6] { -1, -1, -1, -2, -3, -4 };
            matrix[3] = new int[6] { -1, -9, -2, -4, -4, -5 };
            matrix[4] = new int[6] { -7, -3, -3, -2, -9, -9 };
            matrix[5] = new int[6] { -1, -3, -1, -2, -4, -5 };

            Console.WriteLine(hourglassSum(matrix));
            Console.ReadKey();
        }


        static int hourglassSum(int[][] arr)
        {
            int sum = 0;
            int maxSum = 0;
            for (int i = 1; i < arr.Length-1; i++)
            {
                for (int j = 1; j < arr.Length-1; j++)
                {
                    sum = arr[i - 1][j - 1] + arr[i - 1][j] + arr[i - 1][j + 1] +
                                                arr[i][j] +
                            arr[i + 1][j - 1] + arr[i + 1][j] + arr[i + 1][j + 1];
                    if (sum > maxSum)
                        maxSum = sum;

                }
            }
            return maxSum;
        }


        
    }
}
