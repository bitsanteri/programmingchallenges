﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CH7newYearChaos
{
    class Program
    {
        static void Main(string[] args)
        {
            minimumBribes(new int[] { 2, 1, 5, 3, 4 });
            minimumBribes(new int[] { 2, 5, 1, 3, 4 });
            minimumBribes(new int[] { 1, 2, 5, 3, 7, 8, 6, 4 });

            Console.ReadKey();
        }

        static void minimumBribes(int[] q)
        {

            int bribeCnt = 0;

            for (int i = 0; i < q.Length; i++)
            {
                var jumped = q[i] - (i + 1);

                if (jumped > 2)
                {
                    Console.WriteLine("Too chaotic");
                    return;
                }

                //int j = i + 1;
                //int cnt = 0;

                //while (2 > cnt && j < q.Length)
                //{
                //    if (q[j] < q[i])
                //        cnt++;
                //    j++;
                //}

                //bribeCnt += cnt;


                var val = q[i] - 2;
                var start = Math.Max(0, val);

                for (int j = start; j < i; j++)
                    if (q[j] > q[i]) bribeCnt++;

            }

            Console.WriteLine(bribeCnt);
        }
    }
}
